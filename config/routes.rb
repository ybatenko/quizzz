Rails.application.routes.draw do

  root 'dashboard#index'
  get :dashboard, to: 'dashboard#index'

  resource :session, only: [:new, :edit, :create, :destroy]

  resources :quiz do
    member do
      put :start
    end
  end

  namespace :admin do
    get :dashboard, to: 'dashboard#index'
    resources :quizzes do
       resource :assign, only: [:new, :create]
    end
    resources :groups
    resources :users do
      resources :results, except: [:edit, :new, :update]
    end
  end
end
