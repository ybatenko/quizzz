I18n::Backend::Simple.send(:include, I18n::Backend::Pluralization)

I18n.config.available_locales = [:ru, :en]
I18n.default_locale = :ru
