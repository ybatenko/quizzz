class Response
  include Mongoid::Document

  field :body,    type: String
  field :correct, type: Boolean

  scope :correct, -> { where(correct: true) }

  belongs_to :question,
             index: true

end
