class UserAnswer
  include Mongoid::Document
  include Mongoid::Timestamps::Created

  embedded_in :quiz_participation

  # because of embedding it could not be `belongs_to` relation
  field :question_id, type: BSON::ObjectId

  # array of user choosed answer, [BSON::ObjectId] ids
  field :responses_ids, type: Array

  def question
    @_question ||= Question.find(question_id)
  end

  # returns:
  # 0 if no one valid responses given by user
  # 1 if all responses was valid
  # Rational(n/m) if only n valid responses from m
  def check
    correct_responses_ids = question.correct_responses.map(&:_id).sort
    diff = correct_responses_ids - responses_ids.sort

    return 1 if diff.empty? # both ids equals
    return 0 if diff == correct_responses_ids # no one correct result

    total = correct_responses_ids.size
    return Rational(total - diff.size, total) # partially correct
  end
end
