class QuizParticipation
  include Mongoid::Document

  field :started_at,  type: DateTime
  field :finish_at, type: DateTime

  embeds_many :user_answers

  scope :pending,    -> { where(finish_at: nil, started_at: nil) }
  scope :in_process, -> { where(finish_at: nil, :started_at.ne => nil) }
  scope :completed,  -> { where(:finish_at.ne => nil) }

  belongs_to :quiz
  belongs_to :user

  def start!
    self.update_attribute(:started_at, DateTime.now) unless complete?
  end

  def complete?
    return false unless quiz?
    return false unless user_answers?

    (quiz.question_ids - user_answers.map(&:question_id)).empty?
  end

  def complete!
    self.update_attribute(:finish_at, DateTime.now) if complete?
  end

  def score
    sc = user_answers.reduce(0) do |acc, ua|
      acc += ua.check
    end.to_f
    "#{sc} из #{user_answers.count}"
  end
end
