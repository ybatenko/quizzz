# Base abstract class
# Consist of question and one or many correct responses
class Question
  include Mongoid::Document

  field :body, type: String

  belongs_to :quiz,
             index: true

  has_many :responses

  # array with
  # one or many correct responses belonging to this question
  def correct_responses
    self.responses.correct
  end
end
