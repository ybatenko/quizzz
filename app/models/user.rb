class User
  include Mongoid::Document

  field :name,   type: String
  field :role,   type: String, default: 'participant' # or 'admin'
  field :email,  type: String
  field :password_digest, type: String

  has_and_belongs_to_many :user_groups

  has_many :quiz_participations do
    def completed
      where(:finish_at.ne => nil)
    end
    def pending
      where(finish_at: nil, started_at: nil)
    end
    def in_process
      where(finish_at: nil, :started_at.ne => nil)
    end
    def already_participated?(quiz)
      pending.where(quiz_id: quiz.id).exists? ||
        in_process.where(quiz_id: quiz.id).exists?
    end
  end

  def participate_to(quiz)
    if self.quiz_participations.already_participated?(quiz)
      return nil
    end

    self.quiz_participations.create( quiz: quiz )
  end

end
