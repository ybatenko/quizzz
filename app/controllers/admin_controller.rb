class AdminController < ApplicationController
  layout 'admin'
  before_action :require_login, unless: :signed_in?
  before_action :ensure_admin

  private

  def ensure_admin
    unless admin?
      redirect_to :new_session_url, error: 'You should be admin to access this'
    end
  end
end
