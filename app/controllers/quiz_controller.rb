class QuizController < ApplicationController
  before_action :resource

  # PUT
  def start
    resource.start!
    redirect_to edit_quiz_path(resource),
                notice: "Вы начали тест: #{resource.quiz.title}"
  end

  def show
  end

  def edit
  end

  def update
    save_responses

    if resource.finish_at?
      redirect_to root_path,
                  notice: "Вы прошли тест: #{resource.quiz.title}"
    else
      redirect_to root_path,
                  alert: "Вы все еще проходите тест: #{resource.quiz.title}"
    end
  end

  private

  def save_responses
    params[:response].each do |k, v|
      question_oid = BSON::ObjectId.from_string(k)
      if v.is_a?(Array) # radiobutton
        resp_oid = BSON::ObjectId.from_string(v.first)
        resource.user_answers.create(
          question_id: question_oid,
          responses_ids: [resp_oid]
       )
      else # checkboxes
        resource.user_answers.create(
          question_id: question_oid,
          responses_ids: v.values.map{|vv| BSON::ObjectId.from_string(vv)}
       )
      end
    end

    resource.complete!

    resource.reload
  end

  helper_method def resource
    @qp ||= current_user.
              quiz_participations.
              find(params[:id])
  end
end
