class SessionsController < ApplicationController
  skip_before_action :require_login, only: [:new, :edit, :create]

  def new
    render :edit
  end

  def edit
  end

  def create
    user = authenticate_session(session_params)
    if sign_in(user)
      flash.clear
      if admin?
        redirect_to admin_dashboard_url
      else
        redirect_to :root
      end
    else
      if session_params.permitted?
        flash.now[:error] = "Неправильный логин или пароль!"
      end
      render :edit
    end
  end

  def destroy
    sign_out
    redirect_to edit_session_url
  end

  private

  def session_params
    params.permit(:email, :password)
  end
end
