class Admin::AssignsController < AdminController

  def new
    @resource = QuizParticipation.new
    @quiz = Quiz.find(params[:quiz_id])
    @groups = UserGroup.all
  end

  def create
    users = users_to_assign

    unless users
      redirect_to admin_dashboard_path,
                  error: "Не выбраны группы для назначения!"
      return
    end

    assign_to_quiz(users)

    redirect_to admin_dashboard_path,
                notice: "Назначили задание для выбранных групп"
  end

  private

  def assign_to_quiz(users)
    quiz = Quiz.find(params[:quiz_id])
    users.each do |user|
      user.participate_to(quiz)
    end
  end

  def users_to_assign
    return nil unless params[:group]

    UserGroup.find(params[:group].values).
      map(&:users).
      flatten.
      uniq
  end
end
