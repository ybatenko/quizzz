class Admin::QuizzesController < AdminController
  before_action :get_resource, only: [:edit, :update, :destroy]

  def index
  end

  def new
  end

  def create
  end

  def edit
  end

  def update
  end

  def destroy
  end

  private

  def get_resource
    @resource = Quiz.find(params[:id])
  end
end
