class Admin::DashboardController < AdminController

  def index
    @groups = UserGroup.all
    @quizzes = Quiz.all
  end

end
