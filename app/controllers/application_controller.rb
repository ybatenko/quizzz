class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  include Monban::ControllerHelpers

  add_flash_types :warning, :success

  before_action :require_login, unless: :signed_in?

  rescue_from Mongoid::Errors::DocumentNotFound, with: :record_not_found

  private

  def logged_in?
    current_user
  end

  helper_method :current_user

  def require_login
    redirect_to new_session_url # halts request cycle
  end

  helper_method def admin?
    current_user && current_user.role == 'admin'
  end


  def record_not_found
    redirect_to :root_path, error: 'Нет такой записи'
  end
end
