# All passwords are: password
User.create(
  name: 'Admin',
  role: 'admin',
  email: 'admin@quizzz.com',
  password_digest: '$2a$10$zA3LTuubMm71ispcNgMvuOcpaIRRGsoYX4YKKdVUzinkCQ4xfvhQK'
) unless User.where(name: 'Admin').exists?

user_v = User.create(
  name: 'Василий',
  email: 'vasilii@quizzz.com',
  password_digest: '$2a$10$U6Dz0n3HYXRn3WcWAd4hduPr5C6PEromJSJ1do8N7t9JhXhXgiIge'
) unless User.where(name: 'Василий').exists?

user_p = User.create(
  name: 'Петр',
  email: 'piter@quizzz.com',
  password_digest: '$2a$10$AKiXtHvV79GT1c1U/7HxI.ZeRQk2vcwgVcyEhFtDVjG3Ol1jkGrHy'
) unless User.where(name: 'Петр').exists?

user_o = User.create(
  name: 'Ольга',
  email: 'olga@quizzz.com',
  password_digest: '$2a$10$jxoBNrAsip4vxnj8cF46z.xMeHsIDTcs0bQsEi6.GTcCRHgQ7OZva'
) unless User.where(name: 'Ольга').exists?

user_group1 = UserGroup.find_or_create_by( title: 'Котики' )

user_group2 = UserGroup.find_or_create_by( title: 'Зайчики' )

user_group1.users << user_v unless user_group1.users.where(_id: user_v).exists?
user_group1.users << user_p unless user_group1.users.where(_id: user_p).exists?
user_group1.save

user_group2.users << user_p unless user_group2.users.where(_id: user_p).exists?
user_group2.users << user_o unless user_group2.users.where(_id: user_o).exists?
user_group2.save


quiz1 = Quiz.find_or_create_by(
  title: 'Математика',
  description: 'Вопросы, касающиеся математики'
)

QuestionOneCorrect.create(
  quiz: quiz1,
  body: 'Сколько будет: 2 + 2 * 2  ?',
  responses: [
    Response.new(body: '16', correct: false),
    Response.new(body: '6', correct: true),
    Response.new(body: '8', correct: false)
  ]
)

QuestionMultiplyCorrect.create(
  quiz: quiz1,
  body: 'Математически, линия - это ...',
  responses: [
    Response.new(body: 'набор точек на плоскости', correct: false),
    Response.new(body: 'прямая', correct: true),
    Response.new(body: 'черта на какой-нибудь поверхности, узкая полоса', correct: true),
    Response.new(body: 'воображаемая фигура', correct: false)
  ]
)

QuestionOneCorrect.create(
  quiz: quiz1,
  body: 'Лобачевский был ...',
  responses: [
    Response.new(body: 'хипстером', correct: false),
    Response.new(body: 'великим математиком', correct: true),
    Response.new(body: 'создателем машины временем', correct: false),
    Response.new(body: 'такого не было!', correct: false)
  ]
)


quiz2 = Quiz.create(
  title: 'Кибернетика',
  description: 'Вопросы, касающиеся кибернетики'
)

QuestionOneCorrect.create(
  quiz: quiz2,
  body: 'Термин "робот" был придуман ...',
  responses: [
    Response.new(body: 'японцами, еще до второй мировой войны', correct: false),
    Response.new(body: 'Илоном Маском', correct: false),
    Response.new(body: 'писателем Карелом Чапеком', correct: true)
  ]
)

QuestionMultiplyCorrect.create(
  quiz: quiz2,
  body: 'Кто сформировал так называемые четыре закона робототехники?',
  responses: [
    Response.new(body: 'американский фантаст', correct: true),
    Response.new(body: 'В.И. Ленин в 1917г.', correct: false),
    Response.new(body: 'Айзек Азимов в 1942г.', correct: true),
    Response.new(body: 'Джордж Буш, президент США в 1999г.', correct: false)
  ]
)

QuestionOneCorrect.create(
  quiz: quiz2,
  body: 'Робот-пылесос - это ...',
  responses: [
    Response.new(body: 'неадекватный представитель Средней Азии', correct: false),
    Response.new(body: 'помощник по дому', correct: true),
    Response.new(body: 'будущий предводитель восстания машин', correct: false),
    Response.new(body: 'личинка Терминатора', correct: false)
  ]
)
